﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOD_ClassApril.Weather.DataReader
{
    internal class WeatherDataCSVReader: IWeatherReader
    {
        public void Log(WeatherData point)
        {
            throw new NotImplementedException();
        }

        public List<WeatherData> Read(string path)
        {
            // Implementation - data access
            // What if we wanted to add more ways to read data?
            if (!File.Exists(path))
                throw new FileNotFoundException();

            using var reader = new StreamReader(path);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csv.GetRecords<WeatherData>().ToList();
        }
    }
}

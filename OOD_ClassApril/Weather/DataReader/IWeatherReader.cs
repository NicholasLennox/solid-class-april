﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOD_ClassApril.Weather.DataReader
{
    public interface IWeatherReader
    {
        List<WeatherData> Read(string path);
    }
}

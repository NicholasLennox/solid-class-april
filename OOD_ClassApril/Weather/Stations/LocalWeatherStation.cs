﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOD_ClassApril.Weather.Stations
{
    internal class LocalWeatherStation : WeatherStation
    {
        public LocalWeatherStation(string name, string address) : base(name)
        {
            Address = address;
        }

        public string Address { get; }
    }
}

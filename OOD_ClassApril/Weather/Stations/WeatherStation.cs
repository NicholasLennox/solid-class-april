﻿using CsvHelper;
using OOD_ClassApril.Weather.DataReader;
using OOD_ClassApril.Weather.Loggers;
using System;
using System.Collections.Generic;
using System.Formats.Asn1;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOD_ClassApril.Weather.Stations
{
    /// <summary>
    /// This class is responsible for holding and providing weather data.
    /// </summary>
    public class WeatherStation
    {
        public List<WeatherData> Readings { get; private set; } = new List<WeatherData>();
        public string Name { get; }
        private readonly IWeatherLogger _logger;
        private readonly IWeatherReader _reader;

        public WeatherStation(string name, IWeatherLogger logger, IWeatherReader reader)
        {
            Name = name;
            _logger = logger;
            _reader = reader;
        }

        public virtual void ReadWeatherData(string path)
        {
            Readings = _reader.Read(path);
        }

        public void LogWeatherData(WeatherData point)
        {
            _logger.Log(point);
        }
    }
}

﻿using OOD_ClassApril.Weather.DataReader;
using OOD_ClassApril.Weather.Loggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOD_ClassApril.Weather.Stations
{
    internal class RemoteWeatherStation : WeatherStation
    {
        public RemoteWeatherStation(string name, double lat, double @long, IWeatherLogger logger, IWeatherReader reader) : base(name, logger, reader)
        {
            Lat = lat;
            Long = @long;
        }

        public double Lat { get; }
        public double Long { get; }

        public override void ReadWeatherData(string path)
        {
            Console.WriteLine("Connecting to remote server...");
            base.ReadWeatherData(path);
            Console.WriteLine("Reading complete");
        }
    }
}

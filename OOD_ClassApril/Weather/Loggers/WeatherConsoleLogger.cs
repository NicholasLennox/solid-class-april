﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOD_ClassApril.Weather.Loggers
{
    internal class WeatherConsoleLogger : IWeatherLogger
    {
        public void Log(WeatherData point)
        {
            // Implementation - logging
            Console.WriteLine("\nWEATHER LOG ---------------");
            Console.WriteLine($"Date: {point.Date}");
            Console.WriteLine($"Temperature: {point.Temperature}");
            Console.WriteLine($"Humidity: {point.Humidity}");
            Console.WriteLine($"Pressure: {point.Pressure}");
            Console.WriteLine("---------------------------\n");
        }

        public List<WeatherData> Read(string path)
        {
            throw new NotImplementedException();
        }
    }
}

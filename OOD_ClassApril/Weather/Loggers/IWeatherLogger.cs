﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOD_ClassApril.Weather.Loggers
{
    internal interface IWeatherLogger
    {
        void Log(WeatherData point);
    }
}
